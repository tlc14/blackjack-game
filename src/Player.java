import java.util.ArrayList;
import java.util.List;

public class Player {
    private List<Card> hand = new ArrayList<>();


    private int handValue = 0;
    private final int playerID;


    private PlayerState playerState = PlayerState.Hit;


    public Player(int playerID) {
        this.playerID = playerID;
    }
    public int getHandValue() {
        return handValue;
    }
    public PlayerState getPlayerState() {
        return playerState;
    }

    public void addCardToHand(Card card){
        if (getPlayerState() == PlayerState.Hit){
        hand.add(card);
        handValue = handValue + card.getValue();
        }
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }

    public int getID() {
        return playerID;
    }

    public void printHand(){
        for(Card eachCard : hand){
            System.out.println(eachCard.toString());
        }
    }
}
