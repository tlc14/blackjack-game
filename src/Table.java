import java.util.ArrayList;

public class Table {
    private ArrayList<Player> playersOnTable = new ArrayList<>();
    private Deck deck = new Deck();
    private Dealer dealerOnTable;
    public Table(){
        Player player1 = new Player(1);
        playersOnTable.add(player1);
        Player player2 = new Player(2);
        playersOnTable.add(player2);
        Player player3 = new Player(3);
        playersOnTable.add(player3);
        this.dealerOnTable = new Dealer(playersOnTable, deck);
    }
//    public Table(int numberOfPlayers){
//    for (int i= 1; i<= numberOfPlayers; i++){
//        Player player = new Player(i);
//        playersOnTable.add(player);
//    }
//    }

    public Dealer getDealerOnTable() {
        return dealerOnTable;
    }

}
