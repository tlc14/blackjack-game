import java.util.*;

public class Deck {
    private Stack<Card> deck = new Stack<>();
    public Deck(){
        for(Suit s: Suit.values()){
            for (Value v: Value.values()){
                deck.add(new Card(s, v));
            }
        }
    }

    public int getCardCount() {
        return deck.size();
    }

    public Stack<Card> shuffleDeck(){
        Collections.shuffle(deck);
        return deck;
    }


    public Card pop(){
        //try
        return deck.pop();
        //catch EmptyStackException
        // print cards are finished
        //end game
    }

}
