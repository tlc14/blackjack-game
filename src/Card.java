
public class Card {
    private Suit suit;
    private Value value;


    public Card(Suit suit, Value value){
        this.suit = suit;
        this.value = value;

    }
    public int getValue() {
        return value.getNumberValue();
    }

    @Override
    public  String toString(){
        String cardName = value.getNumberValue()+" "+value+ " of "+ suit ;
        return cardName;
    }

}
