import java.util.List;

public class Dealer {
    private Deck gameDeck;
    //Dealer or table has deck? leaning towards table. Then deck is passed to dealer along with player array in  constructor
    private List<Player> playersOnTable;
    private int round;
    private int playersStuck = 0;
    private int playersLeftInGame;
    private int playersGoneBust = 0;
    private boolean hasGameEnded;


    public Dealer(List<Player> playersOnTable, Deck gameDeck) {
        this.gameDeck = gameDeck;
        this.playersOnTable = playersOnTable;
        this.playersLeftInGame = playersOnTable.size();
    }
    public void newGame(){
        gameDeck.shuffleDeck();
    }

    public void runningGame(){
//        checkWinner();
        deal();
        updatePlayerStates();
        checkWinner();
    }



    public void checkWinner(){
        Player winner;
        int tempHandValue = 0;
        //check winner everytime you deal? in deal?
        //if all players are stuck end game
        //if 2 (or all but one) players are bust, declare last player as winner
        //if player is hit or stuck, and players left ==1, winner
        //if player is hit but other players are too, do nothing
        //only reduce maxplayers if gone bust
        //if stuckPlayers+ bustplayers == dealer
        //.getSize of array of players

        for(Player eachPlayer : playersOnTable){
            if (playersStuck == playersOnTable.size()){//refactor to &&
                if (eachPlayer.getHandValue() > tempHandValue){
                    winner = eachPlayer;///wrong logic
                    System.out.println("Game Over. Player" +winner.getID()+ " wins" );
                    hasGameEnded = true;
                }
            } else if (eachPlayer.getHandValue() == 21) {//
                winner = eachPlayer;
                System.out.println("Game Over. Player" +winner.getID()+ " wins" );
                hasGameEnded = true;
                break;
            }else if(playersLeftInGame ==1 && eachPlayer.getPlayerState() != PlayerState.Bust){
                winner = eachPlayer;
                System.out.println("Game Over. Player" +winner.getID()+ " wins" );
                hasGameEnded = true;
                break;
            }else{
                hasGameEnded = false;
            }
        }
    }
    public boolean gameIsStillRunning(){
        return hasGameEnded;
    }

    public void updatePlayerStates(){
        for(Player eachPlayer : playersOnTable){
            if(eachPlayer.getHandValue()<17){
                eachPlayer.setPlayerState(PlayerState.Hit);
            }
            else if(eachPlayer.getHandValue()>17 && eachPlayer.getHandValue()<21){
                eachPlayer.setPlayerState(PlayerState.Stick);
                playersStuck++;
            } else if (eachPlayer.getHandValue()>21) {
                eachPlayer.setPlayerState(PlayerState.Bust);
                playersLeftInGame--;
                //remove from list of players?
            }
        }
        
    }
    public void deal(){
        if(round == 0){
            for (Player eachPlayer : playersOnTable){
                System.out.println("Player"+eachPlayer.getID()+" is dealt:");
                eachPlayer.addCardToHand(this.gameDeck.pop());
                eachPlayer.addCardToHand(this.gameDeck.pop());
                eachPlayer.printHand();
                System.out.println("");

            }
        }else{
            for (Player eachPlayer : playersOnTable){
                if (eachPlayer.getPlayerState() == PlayerState.Hit){
                    System.out.println("Player"+eachPlayer.getID()+" is dealt:");
                    eachPlayer.addCardToHand(this.gameDeck.pop());
                    eachPlayer.printHand();
                    System.out.println("");
                }
            }
        }
        ++round;

    }
//Array of valid players? unnecessary if I check player state first
}
