import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {
    @Test
    public  void getCardCountTest(){
        int expected = 52;
        Deck deck = new Deck();
        int actual = deck.getCardCount();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void shuffleDeckTest(){
        Deck originalDeck = new Deck();
        Deck shuffledDeck = new Deck();
        shuffledDeck.shuffleDeck();
        Assertions.assertNotEquals(originalDeck, shuffledDeck);
    }

}