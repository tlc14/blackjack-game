import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CardTest {
    @Test
    public void cardToStringTest(){
        Card card = new Card(Suit.Hearts, Value.King );
        String expected = "King of Hearts";
        Assertions.assertEquals(expected, card.toString());

    }


}
