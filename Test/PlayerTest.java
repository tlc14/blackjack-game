import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    @Test
    void getHandValueTest() {
        Player player = new Player(1);
        Card card = new Card(Suit.Clubs, Value.Eight);
        //Expected
        player.addCardToHand(card);

        Assertions.assertEquals(8, player.getHandValue());


    }
}