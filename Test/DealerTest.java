import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DealerTest {

    @Test
    void startGame() {
    }

    @Test
    public void dealRoundZeroAndPlayerHitTest() {
        //Given a dealer with a player with hit state
//        Player myPlayer = Mockito.mock(Player.class);
//        Mockito.when(myPlayer.getPlayerState()).thenReturn(PlayerState.Hit);
        Player myPlayer = new Player(1);
        Deck deck = new Deck();
        Dealer dealer = new Dealer(List.of(myPlayer), deck);

        //When I call the deal method
        dealer.deal();
        //Then I should be able to verify that a player has been dealt a card
        Assertions.assertNotEquals(0, myPlayer.getHandValue());
        //And verify that the e=deck no longer contains the card
        Assertions.assertEquals(50, deck.getCardCount());
        //check if deck has reduced by a number of cards and players hand has increased
    }
    @Test
    public void dealNotRoundZeroAndPlayerHitTest() {
        //Given a dealer with a player with hit state
//        Player myPlayer = Mockito.mock(Player.class);
//        Mockito.when(myPlayer.getPlayerState()).thenReturn(PlayerState.Hit);
        Player myPlayer = new Player(1);
        Deck deck = new Deck();
        Dealer dealer = new Dealer(List.of(myPlayer), deck);

        //When I call the deal method
        dealer.deal();
        dealer.deal();
        //Then I should be able to verify that a player has been dealt a card
        Assertions.assertNotEquals(0, myPlayer.getHandValue());
        //And verify that the e=deck no longer contains the card
        Assertions.assertEquals(49, deck.getCardCount());
        //check if deck has reduced by a number of cards and players hand has increased
    }

    @Test
    public void updatePlayerStatesTest(){
        //Given an arraylist of players, and a deck
        Player myPlayer = new Player(1);
        Deck deck = new Deck();
        Dealer dealer = new Dealer(List.of(myPlayer), deck);


        dealer.deal();
        dealer.deal();

        //When a player is ineligible to play for the next round
        //update his state
        dealer.updatePlayerStates();
        //Then verify if state is updated
        Assertions.assertEquals(PlayerState.Bust, myPlayer.getPlayerState());
    }

    @Test
    public void endGameTest(){
        //Given player states

        // When a player has a handvalue  = 17 or all players are bust
        //Then
    }


    @Test
    public void checkWinnerTest(){
        //Given three players and a deck
        Player myPlayer1 = new Player(1);
        Player myPlayer2 = new Player(1);
        Player myPlayer3 = new Player(1);
//
        Deck deck = new Deck();
        Dealer gameDealer = new Dealer(List.of(myPlayer1, myPlayer2, myPlayer3), deck);
        // When a player has won
        gameDealer.newGame();
        gameDealer.runningGame();
        //Then verify that a message is printed and the gameover value is true
//        Assertions.assertEquals(expected, gameDealer.gameIsStillRunning());

    }
}